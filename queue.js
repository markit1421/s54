let collection = [];

// Write the queue functions below.
let print = () => {
	return collection; 
};

let enqueue = (el) => {
	 collection[collection.length]=el;
	 return collection; 
}

let dequeue = () => {
	let newCol=[];
	 let c=0;
	 for(let i=1;i<collection.length;i++){
	 	newCol[c]=collection[i];
	 	c+=1;
	 }
	 collection=newCol;

	 return collection;  
}

let front = () => {
	 return collection[0];  
}

let size = () => {
	return collection.length;  
}

let isEmpty = () => {
	if(collection.length==0) {return true}
	else {return false}
}



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};